#include <boost/config.hpp>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <rapidxml.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/pending/indirect_cmp.hpp>
#include <boost/range/irange.hpp>

#include "room.h"

namespace {
    static const char* const s_usage = "commr <path to .map file> <path to .goal file>";
}

namespace impl {
    std::vector<char> read_file(const char* path) {
        std::ifstream tf(path);
        tf.seekg(0, std::ios::end);
        const size_t sz = tf.tellg();
        std::vector<char> data(sz + 1);
        tf.seekg(0, std::ios::beg);
        tf.read(data.data(), sz);
        data.back() = 0;
        return data;
    }

    typedef std::set<data::room> rooms_t;
    typedef std::map<std::string, unsigned int> objects_map_t;
    typedef std::map<std::string, unsigned int> room_names_map_t;
    typedef std::vector<unsigned int> room_ids_t;
    typedef std::pair<int, int> edge_t;
    typedef std::vector<edge_t> edges_t;
    typedef std::vector<unsigned int> weights_t;
    void load_map(const char* fpath, rooms_t& rooms, objects_map_t& objects, room_names_map_t& rnames, room_ids_t& rids, edges_t& edges, weights_t& weights) {
        using namespace rapidxml;
        xml_document<> doc; 
        std::vector<char> fdata = read_file(fpath);
        doc.parse<0>(fdata.data());
        xml_node<>* root = doc.first_node("map");
        if (!root) {
            throw std::runtime_error("Malformed map xml file");
        }
        for (xml_node<>* rnode = root->first_node("room"); rnode; rnode = rnode->next_sibling("room")) {
            data::room r;
            // To be sure that id is already loaded for edges creation
            xml_attribute<>* id_attr = rnode->first_attribute("id");
            if (!id_attr) {
               throw std::runtime_error("Malformed map xml file");
            }
            const int i = std::atoi(id_attr->value());
            r.id(i);
            for (xml_attribute<>* attr = rnode->first_attribute(); attr; attr = attr->next_attribute()) {
                const char* aname = attr->name();
                if (std::strcmp(aname, "name") == 0) {
                    r.name(attr->value());
                } else if (std::strcmp(aname, "id") != 0) {
                    const int i = std::atoi(attr->value());
                    r.add_door(i, aname);
                    edge_t e = std::make_pair(r.id(), i);
                    edges.push_back(e);
                    weights.push_back(1);
                }
            }
            for (xml_node<>* onode = rnode->first_node("object"); onode; onode = onode->next_sibling()) {
                xml_attribute<>* attr = onode->first_attribute("name");
                const std::string name(attr->value());
                objects.insert(objects_map_t::value_type(name, r.id()));
                r.add_object(name);
            }
            rids.push_back(r.id());
            rnames.insert(room_names_map_t::value_type(r.name(), r.id()));
            rooms.insert(r);
        }
    }

    typedef std::vector<unsigned int> goal_t;
    void load_goal(const char* fpath, const objects_map_t& objects, const room_names_map_t& rnames, goal_t& goal) {
        std::ifstream in(fpath);
        std::string line;
        if (in >> line) {
            room_names_map_t::const_iterator fit = rnames.find(line);
            if (fit == rnames.end()) {
                throw std::runtime_error("Malformed goal file");
            } else {
                goal.push_back(fit->second);
            }
            while (in >> line) {
                objects_map_t::const_iterator fit = objects.find(line);
                if (fit == objects.end()) {
                    throw std::runtime_error("Malformed goal file");
                } else {
                    goal.push_back(fit->second);
                }
            }
        } else {
            throw std::runtime_error("Malformed goal file");
        }
    }

    using namespace boost;
    template < typename time_map_t > 
    class bfs_time_visitor : public default_bfs_visitor {
        typedef typename property_traits < time_map_t >::value_type value_type_t;
        time_map_t m_timemap;
        value_type_t& m_time;
    public:
        bfs_time_visitor(time_map_t tmap, value_type_t& t)
            : m_timemap(tmap)
            , m_time(t) { }

        template < typename vertex_t, typename graph_t >
        void discover_vertex(vertex_t u, const graph_t& g) const {
            put(m_timemap, u, m_time++);
        }
            
    };

    void enter_door(data::room::doors_t::const_iterator it) {
        std::cout << it->second << std::endl;
    }

    typedef rooms_t::const_iterator room_it_t;
    typedef std::stack< room_it_t > path_t;
    data::room::doors_t::const_iterator find_door_to(rooms_t::const_iterator it, unsigned int id, path_t& path) {
        data::room::doors_t::const_iterator dit = it->doors().find(id);
        if (dit == it->doors().end()) {
            if (path.size() > 1u) {
                path.pop();
                rooms_t::const_iterator it_prev = path.top();
                unsigned int id_prev = it_prev->id();
                enter_door(it->doors().find(id_prev));
                dit = find_door_to(it_prev, id, path);
            } else {
                throw std::runtime_error("could not find right door");
            }
        }
        return dit;
    }

    void peek_objects(rooms_t::const_iterator rit) {
        const data::room::objects_t& objects = rit->objects();
        for(data::room::objects_t::const_iterator it = objects.begin(), et = objects.end(); it != et; ++it) {
            std::cout << *it << std::endl;
        }
    }

}


int main(int argc, const char* const argv[]) {
    if (argc < 3) {
        std::cerr << s_usage << std::endl;
        return EXIT_FAILURE;
    }
    try {
        impl::rooms_t rooms;
        impl::objects_map_t objects; 
        impl::room_names_map_t rnames;
        impl::room_ids_t rids;
        impl::edges_t edges;
        impl::weights_t weights;
        impl::load_map(argv[1], rooms, objects, rnames, rids, edges, weights);
        const size_t rsz = rooms.size();
        impl::goal_t goal;
        impl::load_goal(argv[2], objects, rnames, goal);

        using namespace boost;
        typedef adjacency_list < vecS, vecS, directedS > graph_t;
        typedef graph_traits< graph_t >::vertices_size_type vsize_t;
        graph_t g(edges.begin(), edges.end(), vsize_t(rsz));

        typedef graph_traits < graph_t >::vertex_descriptor vertex_descriptor;
        typedef vsize_t* iterator_t;

        std::vector < vsize_t > dtime(num_vertices(g));

         vsize_t time = 0;
         impl::bfs_time_visitor < vsize_t* > vis(&dtime[0], time);
         unsigned int start_id = goal.front();
         breadth_first_search(g, vertex(start_id, g), visitor(vis));

         std::vector< graph_traits<graph_t>::vertices_size_type > discover_order(rsz);
         integer_range < int > range(0, rsz);
         std::copy(range.begin(), range.end(), discover_order.begin());
         std::sort(discover_order.begin(), discover_order.end(),
             indirect_cmp < iterator_t, std::less < vsize_t > >(&dtime[0]));

         
         impl::room_it_t fit_room = rooms.end();
         impl::path_t path;
         for (size_t i = 0u; i < rsz; ++i) {
             unsigned int next_id = rids[discover_order[i]];
             if (i > 0u) {
                data::room::doors_t::const_iterator fit_doors = impl::find_door_to(fit_room, next_id, path);
                impl::enter_door(fit_doors);
            }
            fit_room = rooms.find(next_id);
            impl::peek_objects(fit_room);
            path.push(fit_room);
            if(fit_room == rooms.end()) {
                throw std::runtime_error("smth wrong");
            }
         }

    } catch (const std::exception& ex) {
        std::cerr << "Exception" << ex.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
