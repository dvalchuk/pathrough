#pragma once

#include <string>
#include <map>
#include <vector>

namespace data {

    class room {
        friend bool operator < (const room& a, const room& b);
        friend bool operator == (const room& a, const room& b);
        friend std::ostream& operator << (std::ostream& stream, const room& a);
        enum {
            UNDEFINED_ID = 0u
        };
    public:
        typedef std::map<unsigned int, std::string> doors_t;
        typedef std::vector<std::string> objects_t;
    private:
        unsigned int m_id;
        doors_t m_doors;
        objects_t m_objects;
        std::string m_name;
    public:
        room();
        room(unsigned int id);
        
        unsigned int id() const { return m_id; }
        const doors_t& doors() const { return m_doors; }
        const objects_t& objects() const { return m_objects; }
        const std::string& name() const { return m_name; }

        void id(unsigned int id) { m_id = id; }
        void add_door(unsigned int id, const std::string& name);
        void add_object(const std::string& name);
        void name(const std::string& name) { m_name = name; }
    };

    inline bool operator < (const room& a, const room& b) {
		return a.m_id < b.m_id;
	}

    inline bool operator == (const room& a, const room& b) {
        return a.m_id == b.m_id;
    }

    inline bool operator != (const room& a, const room& b) {
        return !(a == b);
    }

    std::ostream& operator << (std::ostream& stream, const room& a);

}
