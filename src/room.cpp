#include "room.h"

#include <iostream>

namespace data {

    room::room() 
        : m_id(UNDEFINED_ID) {
    }

    room::room(unsigned int id)
        : m_id(id) {
    }

    void room::add_door(unsigned int id, const std::string& name) {
        m_doors.insert(doors_t::value_type(id, name));
    }

    void room::add_object(const std::string& name) {
        m_objects.push_back(name);
    }

    std::ostream& operator << (std::ostream& stream, const room& a) {
        stream << "room[" << a.m_id << "]";
        stream << " name: " << a.m_name;
        for(room::doors_t::const_iterator it = a.doors().begin(), et = a.doors().end(); it != et; ++it) {
            stream << " " << it->second << ": " << it->first;
        }
        stream << " objects: [";
        bool first = true;
        for(room::objects_t::const_iterator it = a.objects().begin(), et = a.objects().end(); it != et; ++it) {
            if (!first) 
                stream << ", ";
            else 
                first = false;
            stream << *it;
        }
        stream << "]";
        stream << std::endl;
        return stream;
    }

}
